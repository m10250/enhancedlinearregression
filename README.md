# EnhancedLinearRegression

This repository contains an enhanced version of the Linear Regression provided by Sklearn, so that it can report T-, P-Values, standard errors and confidence intervals similar to the output of a statsmodels linear regression. An GPU version based on cuML is also available.

## Example output
```
    Coefficients    Standard Errors  T-Values  P-Values   [0.025]   [0.975]
0       152.1335            2.576    59.061     0.000   147.071   157.196
1       -10.0099           59.749    -0.168     0.867  -127.446   107.426
2      -239.8156           61.222    -3.917     0.000  -360.147  -119.484
3       519.8459           66.533     7.813     0.000   389.076   650.616
4       324.3846           65.422     4.958     0.000   195.799   452.970
5      -792.1756          416.680    -1.901     0.058 -1611.153    26.802
6       476.7390          339.030     1.406     0.160  -189.620  1143.098
7       101.0433          212.531     0.475     0.635  -316.684   518.770
8       177.0632          161.476     1.097     0.273  -140.315   494.441
9       751.2737          171.900     4.370     0.000   413.407  1089.140
10       67.6267           65.984     1.025     0.306   -62.064   197.318
```

## Why?

It was noticed that with increasing dataset size (number of observations) or number of variables the computational complexity rises significantly. Sklearn models fit faster than statsmodels models. Performance tests provided in the folder performance tests. Comparision with statsmodels, a wrapped model of statsmodels usable for sklearn from (https://gitlab.com/m10250/sklearn_extensions), sklearn and the introduced enhanced sklearn version. The statsmodels version and sklearn versions performed compared to itself performed similar. 

## Note
The proposed softwarecode builts upon the sklearn linear regression and inherits all features. It has multi-core ability and can, as the sklearn linear regression, be patched with Intel Extension for Scikit-Learn (https://intel.github.io/scikit-learn-intelex/) for increased performance.

## Requirements
scikit-learn \
numpy \
scipy \
pandas \
scikit-learn-intelex (Optional for accelerating CPU Version) \
cuML (Optional for using GPU Version) 

## Credits
The code for the enhanced version was inspired by the following stackoverflow thread: https://stackoverflow.com/questions/27928275/find-p-value-significance-in-scikit-learn-linearregression/42677750#42677750 
