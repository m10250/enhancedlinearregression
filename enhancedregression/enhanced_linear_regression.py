# code based on https://stackoverflow.com/questions/27928275/find-p-value-significance-in-scikit-learn-linearregression/42677750#42677750
from sklearn import linear_model
from scipy import stats
import scipy
import numpy as np
from sklearn.utils.validation import check_is_fitted
from sklearn.feature_selection import f_regression
import pandas as pd

class EnhancedLinearRegression(linear_model.LinearRegression):

    def __init__(self,fit_intercept=True, copy_X=True, n_jobs=None, positive=False): # *args,**kwargs
        super().__init__(fit_intercept=fit_intercept, copy_X=copy_X, n_jobs=n_jobs, positive=positive)

    def fit(self,X,y):
        # basic fit call fro sklearn
        self = super().fit(X,y)
        # include X,y for calculating the statistics 
        # unfortunately necessary, as otherwise fit methods is slow 
        # increases the model size on disk
        self.X = X
        self.y = y
        return self
    
    def summary_(self, names=None):
        """
        Build a nice return pandas dataframe - internal method
        names: alternative list of names to provide starting with the intercept and all coefficients; only used when no names in features_names_in_
        """
        #if check_is_fitted(self) == True:
        
        # Calculate the necessary statistics 
        # This is not included in the fit method to improve the performance -> scipy call computational expensive -> only necessary when the output should be visualized
        
        # Get parameter and intercpet (if applicable)
        if self.fit_intercept==False:
            params = self.coef_
        else:
            params = np.append(self.intercept_,self.coef_)
        predictions = self.predict(self.X)

        if self.fit_intercept == False:
            newX=self.X
        else:
            newX = np.append(np.ones((len(self.X),1)), self.X, axis=1)

        # Degrees of freedom
        df = (len(newX)-newX.shape[1]) # n-k (-1 is included when intercept is set as newX is used including the additional dimension)
        
        # Calculate the MSE
        MSE = (sum((self.y-predictions)**2))/df


        # Calculate variance, standard deviation, T-statistics and p-values
        var_b = MSE*(np.linalg.inv(np.dot(newX.T,newX)).diagonal())
        sd_b = np.sqrt(var_b)
        ts_b = params/ sd_b
        p_values =[2*(1-stats.t.cdf(np.abs(i),df)) for i in ts_b]

        # Calculate the confidence interval of coefficients 
        beta_25 = params-scipy.stats.t.ppf(q=0.975,df=df)*sd_b
        beta_75 = params+scipy.stats.t.ppf(q=0.975,df=df)*sd_b

        # Round
        self.sd_b = np.round(sd_b,3)
        self.ts_b = np.round(ts_b,3)
        self.p_values = np.round(p_values,3)
        self.params = np.round(params,4)
        self.beta_25 = np.round(beta_25, 3)
        self.beta_75 = np.round(beta_75,3)

        # Format everything nicely
        try:
            temp = list(self.feature_names_in_)
            if self.fit_intercept==True:
                names = ["Intercept"] + temp
            else:
                names = temp
            return_frame = pd.DataFrame(self.params,index=names, columns=["Coefficients"])
        except:
            if names == None:
                return_frame = pd.DataFrame(self.params, columns=["Coefficients"])
            else:
                return_frame = pd.DataFrame(self.params, index=names, columns=["Coefficients"])

        return_frame["Standard Errors"] = self.sd_b
        return_frame["T-Values"]=self.ts_b
        return_frame["P-Values"]=self.p_values
        return_frame["[0.025]"]=self.beta_25
        return_frame["[0.975]"]=self.beta_75
        return return_frame
    
    def summary(self, names=None):
        """
        Report details of the model
        """

        return_frame = self.summary_(names=names)
        return return_frame
    
